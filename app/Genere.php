<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genere extends Model
{
    protected $table = "generes";

    protected $fillable = ['genere'];
    public function movies(){
      return $this->hasMany('App\Movie');
    }
}
