<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Session;
use Redirect;
use Front;
class MailController extends Controller
{
    function store(Request $request){
        Mail::send('emails.contact', $request->all(), function($msj){
            $msj->subject('Correo de Contacto');
            $msj->to('emanueljtc@gmail.com');
        });
          flash("¡Se ha enviado de forma exitosa su mensaje!")->success();
          return view('front.contacto');
    }


}
