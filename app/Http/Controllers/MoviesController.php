<?php

namespace App\Http\Controllers;
use App\Genere;
use Illuminate\Http\Request;
use App\Movie;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $movies = Movie::orderBy('id','DESC')->paginate(4);
      $movies->each(function($movies){
          $movies->genere;
       });

      return view('movie.index')
          ->with('movies', $movies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $generes = Genere::pluck('genere', 'id');
      return view('movie.create',compact('generes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $movie = new Movie ($request->all());
        $movie->save();
        flash("¡Se ha registrado la pelicula ". $movie->name ." Correctamente")->success();
        return redirect()->route('movie.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $movie = Movie::find($id);
        $movie->genere;
        $generes = Genere::orderBy('genere', 'DESC')->pluck('genere', 'id');
        return view('movie.edit')
        ->with('movie', $movie)
        ->with('generes', $generes);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $movies = Movie::find($id);
      $movies->fill($request->all());
      $movies->save();

      flash('¡Los Datos Fueron Actualizados de Forma Correcta!')->success();
      return redirect()->route('movie.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $movies = Movie::find($id);
      $movies->delete();

      flash("¡Se ha eliminado la pelicula de forma exitosa!")->error();
      return redirect()->route('movie.index');
    }
}
