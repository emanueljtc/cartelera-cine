<?php

namespace App\Http\Controllers;
use App\Genere;
use Illuminate\Http\Request;
use App\Http\Requests\GeneroRequest;


class GeneresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listing(){
      $generes = Genere::all();
       return response()->json(
          $generes->toArray()
        );
    }
    public function index()
    {
        
        return view('genero.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('genero.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GeneroRequest $request)
    {
        if($request->ajax()){
          Genere::create($request->all());

          return response()->json([
            "mensaje" => "creado"         ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genere = Genere::find($id);

        return response()->json(
        $genere
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $genere = Genere::find($id);
      $genere->fill($request->all());
      $genere->save();

        return response([
          "mensaje" => "Actualizado con Exito"
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $genere = Genere::find($id);
        $genere->delete();


        return response()->json(["mensaje" => "Borrado"]);
    }
}
