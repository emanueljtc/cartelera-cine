<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
class FrontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('front.index');
    }

    public function contacto()
    {
      return view('front.contacto');
    }

    public function reviews()
    {
      $movies = Movie::orderBy('id','DESC')->paginate(3);
      $movies->each(function($movies){
          $movies->genere;
       });

      return view('front.reviews')
          ->with('movies', $movies);
    }
    public function admin()
    {
      return view('admin.index');
    }

}
