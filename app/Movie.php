<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;
class Movie extends Model
{
    protected $table = "movies";

    protected $fillable = [
        'name','cast','direction','duration','path','genere_id'
    ];
    public function setPathAttribute($path){
      if(!empty($path)){
    		$name = Carbon::now()->second.$path->getClientOriginalName();
    		$this->attributes['path'] = $name;
    		\Storage::disk('public')->put($name, \File::get($path));
    }
  }
    public static function Movies(){
      return DB::table('movies')
      ->join('generes', 'generes.id', '=', 'movies.genere_id')
      ->select('movies.*', 'generes.genere')
      ->get();
    }
    public function genere(){
        return $this->belongsTo('App\Genere');
    }
}
