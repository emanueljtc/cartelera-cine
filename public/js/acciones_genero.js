$(document).ready(function(){
  Cargar();
});
function Cargar(){
  var tablaDatos = $('#datos');
  var route = "http://127.0.0.1:8000/generos"

  $("#datos").empty();
  $.get(route, function(res){
     $(res).each(function(key,value){
       tablaDatos.append("<tr><td>"+value.genere+"</td><td><button value ="+value.id+" OnClick='Mostrar(this);' class='btn btn-primary' data-toggle='modal' data-target='#myModal'>Editar</button> <button class='btn btn-danger' value ="+value.id+" OnClick='Eliminar(this);'>Eliminar</button></td></tr>")
     });
  });
}
$('#registro').click(function(){
  var dato = $("#genere").val();
  var route = "http://127.0.0.1:8000/genero";
  var token = $("#token").val();
  $.ajax({
    url: route,
    headers: {'X-CSRF-TOKEN': token},
    type: 'POST',
    dataType: "json",
    data:{genere: dato},
    success:function(data){
      if(data){
            // FUNCION DE Redireccion con tiempo de espera.
        setTimeout("location.href= 'http://127.0.0.1:8000/genero'",2000);
        $("#msj-success1").fadeIn();

      }

    },
    error:function(msj){
      console.log(msj.responseJSON.genere);
      $("#msj").html(msj.responseJSON.genere);
      $("#msj-error").fadeIn();
    }
  })
});

function Mostrar(btn){

  var route= "http://127.0.0.1:8000/genero/"+btn.value+"/edit"

  $.get(route, function(res){
    $('#genere').val(res.genere);
    $('#id').val(res.id);
  });
}
$("#actualizar").click(function(){
  var value = $("#id").val();
  var dato = $('#genere').val();
  var route= "http://127.0.0.1:8000/genero/"+value+"";
  var token = $("#token").val();

  $.ajax({
    url: route,
    headers: {'X-CSRF-TOKEN': token},
    type: 'PUT',
    dataType: 'json',
    data: {genere: dato},
    success: function(){
        Cargar();
        $("#myModal").modal('toggle');
        $("#msj-success2").fadeTo(2000, 500).slideUp(500, function(){
            $("#msj-success2").alert('close');
            });
          }
  });
});
function Eliminar(btn){

  var route= "http://127.0.0.1:8000/genero/"+btn.value+"";
  var token = $("#token").val();

  $.ajax({
    url: route,
    headers: {'X-CSRF-TOKEN': token},
    type: 'DELETE',
    dataType: 'json',
    success: function(){
        Cargar();
        $("#msj-success3").fadeTo(2000, 500).slideUp(500, function(){
            $("#msj-success2").alert('close');
            });
    }
  });
}
