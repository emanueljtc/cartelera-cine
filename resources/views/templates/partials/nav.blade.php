<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{route('users.index')}}">Cinema Admin</a>
    </div>


    <ul class="nav navbar-top-links navbar-right">
         @if (Auth::guest())
            <li><a href="{{ route('login') }}">Login</a></li>
            <li><a href="{{ route('register') }}">Register</a></li>
        @else
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <ul class="dropdown-menu" role="menu">
                    <li>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
        @endif
    </ul>

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li>
                    <a href="#"><i class="fa fa-users fa-fw"></i> Usuario<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="{{route('users.create')}}"><i class='fa fa-plus fa-fw'></i> Agregar</a>
                        </li>
                        <li>
                            <a href="{{route('users.index')}}"><i class='fa fa-list-ol fa-fw'></i> Usuarios</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-film fa-fw"></i> Pelicula<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="{{route('movie.create')}}"><i class='fa fa-plus fa-fw'></i> Agregar</a>
                        </li>
                        <li>
                            <a href="{{route('movie.index')}}"><i class='fa fa-list-ol fa-fw'></i> Peliculas</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="#"><i class="fa fa-child fa-fw"></i> Genero<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="{{route('genero.create')}}"><i class='fa fa-plus fa-fw'></i> Agregar</a>
                        </li>
                        <li>
                            <a href="{{route('genero.index')}}"><i class='fa fa-list-ol fa-fw'></i> Generos</a>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
    </div>

</nav>
