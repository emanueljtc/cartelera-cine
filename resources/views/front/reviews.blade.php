@extends('templates.principal')
	@section('content')
				<div class="review-content">
							<div class="top-header span_top">
								<div class="logo">
									<a href="index.html"><img src="images/logo.png" alt="" /></a>
									<p>Movie Theater</p>
								</div>
								<div class="search v-search">
									<form>
										<input type="text" value="Search.." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search..';}"/>
										<input type="submit" value="">
									</form>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="reviews-section">
								<h3 class="head">Movie Reviews</h3>
									<div class="col-md-9 reviews-grids">
										@foreach($movies as $movie)
												<div class="review">
													<div class="movie-pic">
														<img src="movies/{{ $movie->path }}" alt="">
													</div>
													<div class="review-info">
														<a class="span" href="single.html"><i>{{ $movie->name}}</i></a>
														<p class="info">CAST:&nbsp; &nbsp;{{ $movie->cast}}</p>
														<p class="info">DIRECTION: &nbsp;&nbsp;{{ $movie->direction}}</p>
														<p class="info">GENERE:&nbsp;&nbsp;{{ $movie->genere->genere}}</p>
														<p class="info">DURATION:&nbsp;&nbsp;{{ $movie->duration}} minutos</p>
													</div>
											<div class="clearfix"></div>
										@endforeach
										</div>
									</div>

							</div>
							<br>
							{{ $movies->render()}}
			</div>
			<div class="clearfix"></div>
		<div class="review-slider">
			 <ul id="flexiselDemo1">
				 @foreach($movies as $movie)
				<li><img src="movies/{{ $movie->path }}" alt=""></li>
				@endforeach

			</ul>
		</div>
	@endsection
