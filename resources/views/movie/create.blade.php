@extends('templates.admin')
@section('title', 'Registrar Pelicula')
@section('content')
{!! Form::open(['route' => 'movie.store', 'method' => 'POST','files'=>'true'])!!}  
    @include('movie.partials.form_create')
    {!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}
  {!!Form::close()!!}    
@endsection
