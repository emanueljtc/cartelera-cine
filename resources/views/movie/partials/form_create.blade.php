

    <div class="form-group">
    {!!Form::label('nombre','Nombre:')!!}
    {!!Form::text('name',null,['class'=>'form-control', 'placeholder'=>'Ingresa el Nombre de la pelicula'])!!}
    </div>
    <div class="form-group">
    {!!Form::label('Elenco','Elenco:')!!}
    {!!Form::text('cast',null,['class'=>'form-control', 'placeholder'=>'Ingresa el elenco'])!!}
    </div>
    <div class="form-group">
    {!!Form::label('Direccion','Dirección:')!!}
    {!!Form::text('direction',null,['class'=>'form-control', 'placeholder'=>'Ingresa al director'])!!}
    </div>
    <div class="form-group">
    {!!Form::label('Duracion','Duración:')!!}
    {!!Form::time('duration',null,['class'=>'form-control', 'placeholder'=>'Ingresa la duración'])!!}
    </div>
    <div class="form-group">
    {!!Form::label('Poster','Poster:')!!}
    {!!Form::file('path',['id' => 'input-id','class' =>'file', 'data-preview-file-type' => 'text'])!!}
    </div>
    <div class="form-group">
    {!!Form::label('Genero','Genero:')!!}
    {!!Form::select('genere_id',$generes, null, ['class' => 'form-control select-category ', 'placeholder' => 'Seleccione un genero', 'required']) !!}
    </div>

@section('js')
  <script>

      $('.textarea-content').trumbowyg();
      $('#input-id').fileinput();
  </script>
@endsection
