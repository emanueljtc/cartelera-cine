@extends('templates.admin')
@section('title', 'Registrar Pelicula')
@section('content')
  {!! Form::model($movie,['route' => ['movie.update',$movie->id],'method' => 'PUT','files'=>'true'])!!}
      @include('movie.partials.form_create')
    {!!Form::submit('Actualizar',['class'=>'btn btn-primary'])!!}
  {!!Form::close()!!}

@endsection
