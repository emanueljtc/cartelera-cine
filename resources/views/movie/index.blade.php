@extends('templates.admin')
@section('title', 'Lista Usuarios')
@section('content')
<table class="table table-striped">
  <thead>
    <th>Nombre</th>
    <th>Genero</th>
    <th>Direccion</th>
    <th>Caratula</th>
    <th>Operaciones</th>
  </thead>
    @foreach($movies as $movie)
      <tbody>
        <td>{{ $movie->name }}</td>
        <td>{{ $movie->genere->genere }}</td>
        <td>{{ $movie->direction }}</td>
        <td>

          <img src="movies/{{ $movie->path }}" alt="" style="width:100px">

        </td>
        <td>
        <a href="{{ route('movie.edit', $movie->id) }}" class="btn btn-warning"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>

        <a href="{{ route('movie.destroy', $movie->id) }}" onclick="return confirm('¿Seguro que deseas eliminarlo?')" class="btn btn-danger"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a>
      </td>
      </tbody>
   @endforeach
</table>
{!!$movies->render()!!}
@endsection
