@extends('templates.admin')
@section('title', 'Usuarios')
@section('content')
  <div class="users">
    @include('users.partials.list_users')
  </div>
@endsection
