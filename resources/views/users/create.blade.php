@extends('templates.admin')
@section('title', 'Crear Usuario')
@section('content')
  <br>
  {!! Form::open(['route' => 'users.store', 'method' => 'POST'])!!}

        <div class="form-group ">
            {!!Form::label('Nombre')!!}
            {!!Form::text('name', null,['class'=>'form-control', 'placeholder'=>'Ingresa el nombre del usuario'])!!}
        </div>
        <div class="form-group ">
            {!!Form::label('Correo')!!}
            {!!Form::text('email', null,['class'=>'form-control', 'placeholder'=>'Ingresa el correo'])!!}
        </div>
        <div class="from-group ">
            {!! Form::label('Password') !!}
            {!! Form::password('password', ['class'=>'form-control', 'placeholder'=>'ingresa una password']) !!}
        </div>
        <br>
        {!! Form::submit('Registrar', ['class'=>'btn btn-primary']) !!}

  {!! Form::close() !!}
@endsection
