@extends('templates.admin')
@section('title', 'Editar Usuario')
@section('content')
  <br>
  {!! Form::open(['method' => 'PATCH', 'action' => ['UsersController@update', $user->id] ])!!}

        <div class="form-group ">
            {!!Form::label('Nombre')!!}
            {!!Form::text('name', $user->name,['class'=>'form-control', 'placeholder'=>'Ingresa el nombre del usuario'])!!}
        </div>
        <div class="form-group ">
            {!!Form::label('Correo')!!}
            {!!Form::text('email', $user->email,['class'=>'form-control', 'placeholder'=>'Ingresa el correo'])!!}
        </div>

        <br>
        {!! Form::submit('Actualizar', ['class'=>'btn btn-primary']) !!}

  {!! Form::close() !!}
@endsection
