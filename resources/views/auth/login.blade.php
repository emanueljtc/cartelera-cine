@extends('templates.principal')

@section('content')
  <div class="header">
  <div class="top-header">
  <div class="logo">
    <a href="index.html"><img src="images/logo.png" alt="" /></a>
    <p>Movie Theater</p>
  </div>
  <div class="search">
    <form>
      <input type="text" value="Search.." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search..';}"/>
      <input type="submit" value="">
    </form>
  </div>
  <div class="clearfix"></div>
  </div>
  <div class="header-info">
  <h1>BIG HERO 6</h1>
  @include('flash::message')
  {!!Form::open(['route'=>'login', 'method'=>'POST'])!!}
    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
        {!!Form::label('correo', 'Correo')!!}
        {!!Form::email('email',null,['class'=>'form-control',
          'placeholder'=>'Ingresa Tu Correo'])!!}
          @if ($errors->has('email'))
              <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
              </span>
          @endif
    </div>
    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
      @if ($errors->has('password'))
        <span class="help-block">
          <strong>{{ $errors->first('password') }}</strong>
        </span>
      @endif
        {!!Form::label('contraseña', 'Contraseña')!!}
        {!!Form::password('password',['class'=>'form-control',
          'placeholder'=>'Ingresa Tu Contraseña'])!!}
    </div>
    {!! Form::submit('Iniciar',['class'=>'btn btn-block btn-primary btn-primary'])!!}

  {!!Form::close()!!}
                  </div>
            		</div>
            		<div class="review-slider">
            			 <ul id="flexiselDemo1">
            			<li><img src="images/r1.jpg" alt=""/></li>
            			<li><img src="images/r2.jpg" alt=""/></li>
            			<li><img src="images/r3.jpg" alt=""/></li>
            			<li><img src="images/r4.jpg" alt=""/></li>
            			<li><img src="images/r5.jpg" alt=""/></li>
            			<li><img src="images/r6.jpg" alt=""/></li>
            		</ul>

            		</div>
@endsection
