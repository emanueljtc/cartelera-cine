@extends('templates.admin')
  @section('title','Crear Genero')



@section('content')

    {!!Form::open()!!}

      <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        @include('genero.form.genero')
          {!!link_to("#", $title='Registrar', $attributes = ['id'=>'registro',
            'class'=>'btn btn-primary col-md-3 col-md-offset-5'], $secure= null)!!}


    {!!Form::close()!!}
@endsection

  @section('scripts')
    <script src="{{ asset('js/acciones_genero.js') }}"></script>
  @endsection
