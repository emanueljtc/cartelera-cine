<br>
<div id="msj-success1" class="alert alert-success alert-dismissible" role="alert" style="display:none">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Genero Agregado Correctamente</strong>
</div>
<div id="msj-success2" class="alert alert-success alert-dismissible" role="alert" style="display:none">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <strong>Genero Actualizado Correctamente.</strong>
</div>
<div id="msj-success3" class="alert alert-success alert-dismissible" role="alert" style="display:none">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <strong>Genero Eliminado Correctamente.</strong>
</div>
<div id="msj-error" class="alert alert-danger alert-dismissible" role="alert" style="display:none">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong id="msj"></strong>
</div>
