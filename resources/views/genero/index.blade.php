@extends('templates.admin')
  @section('title','Lista de Generos')


    @section('content')
      @include('genero.modal')
      <a href="{{ route('genero.create') }}" class="btn btn-info">Registrar Nuevo Genero</a><hr>

        <table class="table table-striped">
            <thead>
              <th>Nombre</th>
              <th>Acciones</th>
            </thead>
            <tbody id="datos">

            </tbody>
        </table>

    @endsection
    @section('scripts')
      <script src="{{ asset('js/acciones_genero.js') }}"></script>
    @endsection
