<?php


//RUTA BASICA
      // Route::get('prueba', function(){
      //     return 'BIENVENIDO A RUTA DE PRUEBA';
      // });
//RUTA POR PARAMETROS
      // Route::get('nombre/{nombre}', function($nombre){
      //   return "Mi nombre es: ". $nombre;
      // });
// RUTA POR PARAMETROS CON VALOR POR DEFECTO
    // Route::get('usuario/{name?}', function($name = 'LordSystem'){
    //       return "Tu nombre es: ". $name;
    // });
//RUTAS Front
Route::get('/', 'FrontController@index');
Route::get('contacto', 'FrontController@contacto');
Route::get('reviews', 'FrontController@reviews');
Route::resource('genero', 'GeneresController');
Route::resource('mail', 'MailController');


//RUTAS DEL ADMIN
Auth::routes();
Route::group(['middleware' => ['auth','admin']], function(){
        Route::get('admin', 'FrontController@admin');
        Route::resource('movie', 'MovieController');
        Route::get('movie/{id}/destroy',[
          'uses' => 'MovieController@destroy',
          'as' => 'movie.destroy'
        ]);
        Route::resource('users', 'UsersController');
        Route::get('users/{id}/destroy',[
          'uses' => 'UsersController@destroy',
          'as' => 'users.destroy'
        ]);
        Route::get('generos', 'GeneresController@listing');
        Route::get('genero/{id}/destroy',[
          'uses' => 'GeneresController@destroy',
          'as' => 'generes.destroy'
        ]);

  });

//Route::get('/home', 'HomeController@index')->name('home');
